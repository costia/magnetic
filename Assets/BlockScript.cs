﻿using UnityEngine;
using System.Collections;

public class BlockScript : MonoBehaviour {
	public int type;
	public float mCharge;
	public Vector3 direction;
	public int directional;
	
	// Use this for initialization
	void Start () {
		switch (type){
			case 0:
			this.GetComponent<Renderer>().material.color=Color.gray;
				break;
			case 1:
				this.GetComponent<Renderer>().material.color=Color.red;
				break;
			case -1:
				this.GetComponent<Renderer>().material.color=Color.blue;
				break;
		}
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
