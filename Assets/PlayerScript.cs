﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {
	bool[] keyPressed=new bool[3];
	float mMaxForce=5.0f;
	
	
	int mType;
	// Use this for initialization
	void Start () {
		mType=0;
		this.GetComponent<Renderer>().material.color=Color.white;

	}
	
	// Update is called once per frame
	void Update () {
		Object[] tmp=GameObject.FindObjectsOfType(typeof(BlockScript));
		foreach(object thisObject in tmp){
			BlockScript bs=(BlockScript)thisObject;
			Vector3 diff=(this.transform.position-bs.transform.position);
			float force=bs.mCharge/Mathf.Sqrt(diff.sqrMagnitude);
			if ((bs.type*mType)<0){
				diff=-diff;
			}else if((bs.type*mType)==0){
				force=0;
			}
			
			if (force>mMaxForce) force=mMaxForce;
			
			if (bs.directional==1){
				if (Mathf.Sign(Vector3.Dot(diff,bs.direction))>0){
					diff=bs.direction;
				}else{
					diff=Vector3.zero;
				}
			}else if (bs.directional==2){
				if (Mathf.Sign(Vector3.Dot(diff,bs.direction))>0){
					diff=bs.direction;
				}else{
					diff=-bs.direction;
				}
			}
			diff.Normalize();
			diff.Scale(new Vector3(force,force,force));
			this.GetComponent<Rigidbody>().AddForce(diff);
		}
		
		mType=0;
		if (Input.GetMouseButton(0)){
			mType++;
		}
		if (Input.GetMouseButton(1)){
			mType--;
		}
		switch (mType){
			case 0:
				this.GetComponent<Renderer>().material.color=Color.grey;
				break;
			case 1:
				this.GetComponent<Renderer>().material.color=Color.red;
				break;
			case -1:
				this.GetComponent<Renderer>().material.color=Color.blue;
				break;
		}
	}
	
	void FixedUpdate (){
	}
}
